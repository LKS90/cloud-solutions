Hands-On: Hello (Cloud) World
-----------------------------

A1: Registrieren Sie sich bei „Ihrem“ Cloud-Provider. Installieren Sie zudem
allfällige Tools: Je nach Provider ist das eine Kombination aus IDE-Plugins
oder Command Line Tools (Sie erinnern sich an die Übung von Woche 1).

https://flow.ch/appengine

![Index von flow.ch/appengine](Flow_Index.PNG)

##### Hello Cloud Tutorial
Als Einstieg in die Umgebung der Flow App Engine and Elastic werden wir einen kleinen Spring Boot Webservice erstellen.
Als Tools werden wir hierbei die Web Oberflaeche zur Konfiguration des Environemnts benutzen sowie das 
jelastic-maven-plugin zum Deployen.
Die Applikation selbst ist eine gewoehnliche Spring Boot Applikation.

###### 1 Create New Environment
![](a1/flow_hello_1_create_env.png)
###### 2 Choose Environment Settings
![](a1/flow_hello_2_create_env_settings.png)
###### 3 Create a Spring Boot App
Um eine Spring Boot App zu erstellen, kann ein beliebiges Tutorial aus dem Internet verwendet werdne. Fuer dieses
Beispiel wurde das folgende verwendet:
https://spring.io/guides/gs/spring-boot/

###### 4 Add jelastic-maven-plugin for Deployment
Damit die Applikation nicht von Hand als *.jar File deployed werden muss, verwenden wir das jelastic-maven-plugin
welches diesen Schritt als Maven Goal automatisiert. Hierzu fuegen wir in der pom.xml das plugin sowie das Repository 
 ein:

```xml
<project>

  ...
  
  <pluginRepositories>
    <pluginRepository>
       <id>sonatype-oss-public</id>
       <url>https://oss.sonatype.org/content/groups/public</url>
       <releases>
           <enabled>true</enabled>
       </releases>
       <snapshots>
           <enabled>true</enabled>
       </snapshots>
    </pluginRepository>
  </pluginRepositories>
  
  <build>
        <plugins>
            <plugin>
                <groupId>com.jelastic</groupId>
                <artifactId>jelastic-maven-plugin</artifactId>
                <version>1.9.3</version>
                <configuration>
                    <email>cedric.wehli@hsr.ch</email> <!--your Jelastic account login (email)-->
                    <password>*********</password> <!--your Jelastic account password-->
                    <artifact>cloudsolutionsTUT.jar</artifact> <!--artifact to be deployed-->
                    <context>ROOT</context> <!--preferable context name (ROOT if skipped)-->
                    <environment>cloudsolutions02</environment> <!--name of a target Jelastic environment-->
                    <comment>testtesttest</comment> <!--custom comment, if needed-->
                    <api_hoster>app.appengine.flow.ch</api_hoster> <!--domain name of your platform-->
                </configuration>
            </plugin>
            
            ...
            </plugins> 
    </build>
</project>
```

###### 5 Deploy The Hello Cloud Service
* Deploy the app
```
mvn jelastic:deploy -Djelastic.password={password}
```
* Re-Deploy the app
```
mvn clean install jelastic:deploy
```

###### 6 Visit your Service
Dein Service steht unter der bei der Initialisierung des Environments gewaehlten Namen zur Verfuegung.


**- Weshalb haben Sie diesen Provider für die Evaluation ausgewählt?**

* Schweizer Anbier
* Uebersichtliches Angebot (nicht zu gross)
* Jelastic als bekanntes Framework

**- Welche Möglichkeiten haben Sie, die Applikation zu starten und zu stoppen?**

* Im Browser

![](a1/flow_startstop_browser_start.png)
![](a1/flow_startstop_browser_stop.png)
* Per CLI
```
~/jelastic/environment/control/stopenv --envName {env_name} 
~/jelastic/environment/control/startenv --envName {env_name}
```
* Per API
```
https://[hoster-api-host]/1.0/environment/control/rest/startenv?envName=[string]&session=[string]
https://[hoster-api-host]/1.0/environment/control/rest/stopenv?envName=[string]&session=[string]
```

**- Wo können Sie das Logfile der Applikationbetrachten?**
* Browser

![](a1/flow_log_browser_application.png)
![](a1/flow_log_browser_node.png)
* API
```
https://[hoster-api-host]/1.0/environment/control/rest/getlogs?path=[string]&envName=[string]&session=[string]&nodeId=[int]
```

**- Worin sehen Sie die Vor-und Nachteile Ihres Providers verglichen mit
Google App Engine(GAE)?**

* (+) Kein Vendor-Lockin (Jelastic gibt's von verschiedenen Anbietern)
* (+) Kleineres Angebot (Muss nicht alles koennen)
* (+) Open Source Framework 
* (+) Standort Schweiz (Support, (?) Datenschutz)
* (-) Kleineres Angebot, muss zu den Anforderungen passen
* (-) Wer weiss, ob es Flow morgen noch gibt?
* (?) Preis 

Analyse: OSSM-Definition
------------------------

A2:  In der ersten Woche haben Sie die OSSM-Kriterien kennengelernt und auf GAE
angewandt. Wiederholen Sie die Analyse nun mit Ihrem Cloud Provider.

- On-Demand: erfüllt ihr Provider dieses Kriterium? Woran merkt man das als
  Cloud-User?

Man kann sich mit einer Email und ohne Kreditkarte automatisch registrieren,
woraufhin man Zugriff auf ein Dashboard erhält. Auf diesem Dashboard können
nun Umgebungen gestartet und konfiguriert werden. Bei bedarf kann man diese
Umgebungen von dort auch stoppen oder die Konfiguration anpassen, falls die
automatische Skalierung an das dort definierte Limit stösst.

- Was für Möglichkeiten des Self-Service werden geboten?

Neue Umgebungen mit den unterschiedlichsten Runtimes können per Web Oberfläche
mit simplen Clicks ausgewählt werden. Zusätzlich gibt es eine Docker Container
Repository, von wo aus man komplett selbs-tdefinierte Umgebungen mit den 
benötigten Komponenten zusammenstellen kann. Pre- und Post-Hooks erlauben die nutzung von Standardkomponenten mit seinen eigenen Modifikationen.

- Wie schätzen Sie die Skalierbarkeit des Cloud-Offerings ein?
  (Granularität, Zeitpunkt)?

Die kleinste Einheit eines Servers, welche Jelastic anbietet, nennt sich
Cloudlet. 200MHz und 128MB sind die Spezifikationen eines solchen Cloudlets.
Pro Umgebung kann eine Mindestanzahl an Cloudlets reserviert werden, für die
Skalierung kann ein oberes Limit eingestellt werden. Eine Umgebung kann maximal
128 Cloudlets in Anspruch nehmen, wobei diese aus maximal 48 Nodes mit 16 Nodes
pro Layer bestehen darf.
Ein Layer (auch Node Group genannt) fasst Nodes mit ähnlichen Aufgaben zusammen
(Load Balancing, Cache, Storage, Build Nodes, VPS, etc.).

- Ist das Measurable-Kriterium erfüllt? Wo finden sich die entsprechenden Informationen?

Für jeden Tag wird die durchschnittliche Nutzung jeder Umgebung in der Billing
History aufgelistet. Pro Umgebung wird die Zusammensetzung der Kosten durch die
Node Groups aufgelistet und ein Preis mit Total angezeigt. Beim Erstellen von
Umgebungen wird bereits versucht, die Kosten abzuschätzen. Es kann ein maximum
Festgelegt werden, wodurch überdurchschnittliche Kosten durch Skalierung
verhindert werden kann.

![Übersicht Kosten Flow Appengine](Flow_Cost_Overview.PNG)

D: Beantworten Sie die obigen Fragen mit jeweils 3-4 Sätzen und allenfalls
Screenshots (z.B. für Measurable)

Konzept: Cloud Computing Patterns
---------------------------------

A3: Erstellen Sie eine einfache Visualisierung Ihrer Hello World-Anwendung mit
den Cloud Computing Patterns. Dabei können Sie folgende Punkte berücksichtigen:

- Welches Pattern aus der Process Offerings Kategorie 1 realisiert der Provider?
  Falls es mehrere gibt, wählen Sie das zur Hello World-Anwendung passendste
  aus.

Es handelt sich bei Jelastic um einen Elastic Platform Service. Dieser bietet eine Execution Environment an welche die einzelnen Komponenten je nach Auslastung skaliert.

![Übersicht beim erstellen einer neuen Umgebung bei Flow](Flow_Setup_Dashboard.PNG)

- Für welches der Workload Patterns eignet sich Ihr Provider bzw. Ihr Offering
  am besten?

Da Jelastic nur für benutzte Resourcen abrechnet und sehr Skalierbar ist,
sollte es sich im Grunde für jede Art von Workload Pattern eignen. Falls die
Auslastung der Applikation sehr hoch sein sollte, könnte Flow unter Umständen nicht hoch genug skalieren. Allerdings ist das Maximum erst bei 51,2GHz und 128GB Arbeitsspeicher erreicht.

![Übersicht Workload Flow](Flow_Workload.PNG)

- Welche Komponenten stellt der Cloud-Provider in den von Ihnen evaluierten Cloud
  Offerings$^2$ zur Verfügung?

Die Verfügbarkeit von Komponenten hängt von Kompatibilitäten ab. Jede Art Komponente
(Node) kann mehrmals vorkommen.

Balancer:  
NGINX, Apache Balancer, HAProxy und Varnish

Application Server:  
**Java** - Tomcat, GlassFish, Java Engine, Jetty, Payara, SmartFoxServer 2X,
Spring Boot, TomEE+, WildFly, NGINX  
**JavaScript** - Node.JS  
**Go** - Golang Server  
**Python** - Apache  
**PHP**, **Ruby** - NGINX, Apache

Caching:  
Memcached

Dies ist ein in-Memory Hash-based Key-Value Storage, welcher, falls nötig, auf
mehrere Systeme skaliert werden kann.

Storage:  
MariaDB, MySQL, Percona, PostgreSQL, MongoDB, Couchbase, CouchDB, Redis, Shared Storage

Dies sind Relationale Datenbanken, Blob Storages und Key-Value Storages.

VPS:  
CentOS 7.2 oder Ubuntu 16.04

Hypervisor, Server ist jedoch Teil der Execution Environment.

Build node:  
Maven

Die meisten Komponenten stehen in verschiedenen Versionen zur Verfügung.
Zusätzlich können Docker Container für alle Arten von Nodes eingesetzt werden.

D: Eine Visualisierung, und beantworten Sie die obigen Fragen mit einem Absatz
(3-4 Sätze) pro Frage.


Hands-On: Self-Information
--------------------------

A4: Nachdem Sie mit der Hello-World Anwendung erste Erfahrungen sammeln konnten,
geht es jetzt darum, eine bestehende Applikation zu deployen, und zwar die aus
der ersten Übung bekannte SelfInformation-Applikation. Importieren Sie das
Projekt in die IDE Ihrer Wahl und versuchen Sie, die Applikation zu deployen.
Dokumentieren Sie dabei allfällige Hindernisse und beantworten Sie folgende
Fragen:

- Wieviel RAM steht Ihrer Cloud-Instanz maximal zur Verfügung?
- Von welchem Hersteller stammt die JVM?
- Welche IP-Adresse hat der Server laut der Self-InformationApplikation?

Analyse: Preisrecherche
-----------------------

A5: Beantworten Sie die folgenden Fragen: 
- Welche Preismodelle stehen zur Verfügung (Bsp. Subscription, Flat Rate, Pay-per-Use)?

Flow bietet Pay-per-Use sowie einen Fixpreis an. Beim Fixpreis wird pro Instanz
abgerechnet, es findet keine automatische Skalierung zum Einsparen von Resourcen
statt, genauso kann nicht weiter nach oben skaliert werden, wenn die bezahlten
Ressourcen nicht für die momentane Last ausreichen. 

- Wie kann bezahlt werden (Kreditkarte, Rechnung, ...)?

Kreditkarte und vorauszahlung via Bank Transfer sind möglich. Für die Einzahlung
per Bank muss man jedoch den Anbieter im Vorraus anschreiben, was nicht wirklich
dem Self-Service Konzept der OSSM-Definition entspricht.

- Gibt es ein Gratis-Angebot für Einsteiger(falls ja:was sind die Einschränkungen)?

Man kann sich kostenlos ohne Angabe einer Kreditkarte registrieren und alle
Funktionen 14 Tage lang kostenlos testen. Jedoch gibt es Einschränkungen im
Vergleich mit dem bezahlten, vollwertigen Konto.

![Vergleich Trial vs. zahlender Kunde](Flow_Trial_Limits.PNG)
![Vergleich Trial vs. zahlender Kunde ff.](Flow_Trial_Limits_2.PNG)

- Welche Ressourcen werden abgerechnet?

Abgerechnet wird pro Cloudlet welches in einer Stunde genutzt wird.

![Preis für ein Cloudlet](Flow_Cloudlet.PNG)

- Was kostet Sie das Hosting der Anwendung aus der Ausgangslage?

76Rp. pro Tag bei minimaler Auslastung.
Im Monat entstehen dadurch vorraussichtlich Kosten in höhe von etwa **22.80 CHF**. 

![Preis der Self-Info Applikation pro Tag](Flow_Price_Selfinfo.PNG)

- Vergleichen Sie das im vorherigen Schritt gefundene Angebot mit Amazon Web Services (AWS) Elastic Compute Cloud (EC2) anhand des AWS-Preiskalkulators.

Bei einer minimal konfigurierten Umgebung, welche in etwa der von Flow entspricht,
entstehen laut Rechner von Amazon schätzungsweise Kosten in Höhe von 10-15$.
Zur Zeit entspricht dies in etwa 10-15CHF im Monat.

![Abschätzung Self-Info kosten laut Preiskalkulator AWS](AWS_Comparison.PNG)

- Welche Schwierigkeiten treten bei den *(sic.)* Vergleich auf (z.B.
unterschiedliche Servicemodelle oder Qualitätseigenschaften, versteckte Kosten,
widersprüchliche Angaben)?

Im AWS Preiskalkulator muss man sich explizit für eine grösse von Server
entscheiden. Einen Server, welcher direkt mit einem Cloudlet vergleichbar ist,
gibt es nicht. Daher wurde für den Balancer ein `t3.nano` gewählt und als
Applikationsserver die Variante `t2.small`.  
Die Kosten im laufenden Betrieb könnten mit steigender Auslastung bei Flow
(wenn kein Limit gesetzt ist) weitaus höher ausfallen. Mit einem vollwertigen
Account könnte man auch noch höher Skalieren, wodurch wiederum höhere Kosten
enstehen würden. In der Preiskalkulation von AWS wären die Server zu klein
Dimensioniert und daher ist eine Vorraussage nur mit Abschätzungen möglich.

Inbound Traffic ist bei AWS kostenfrei, Outbound Traffic wird ab 1GB pro Monat
und Region verrechnet. Flow gibt keine Kosten für Traffic an, *scheinbar* wird
Traffic nicht berechnet.

### Beispiel maximale Kosten:  
Maximale Auslastung Flow - geschätzte Kosten monatlich: 137CHF  
Vergleich AWS - geschätzte monatliche Kosten: 110$ / 110CHF.

![Abschätzung maximale Kosten Flow](Flow_Max_Cost.PNG)
![Abschätzung maximale Kosten AWS](AWS_Max_price.PNG)

Bei dieser Abschätzung wurden die Server ebenfalls höher gewählt, um in etwa
der Leistung von 16 Cloudlets zu entsprechen. Jedoch ist ein Eins zu Eins
Vergleich nicht möglich, da die effektive Auswahl von Servern AWS überlassen
bleibt.


Analyse: Preisvergleich eines Hosting, IaaS und Paas
----------------------------------------------------

A6: Kalkulieren Sie die Beschaffungs-und Betriebskosten für zwei Jahre bei den
Hostingoptionen  
a) klassisch mit eigenem Server (Dedicated Local Hosting):

Beschaffung:  
- Hardware: [HPE ProLiant MicroServer Gen10 (AMD Opteron X3216, 8GB, 3.5")](https://www.digitec.ch/de/s1/product/hpe-proliant-microserver-gen10-amd-opteron-x3216-8gb-35-server-6359954?supplier=406802&tagIds=615-91)  - **390CHF**
- Setup: ~ 1 Tag, etwa **1300CHF** ($8 * 165$CHF)

Betrieb (24 Monate):  
- Internetanschluss: InOne SME, ~130CHF pro Monat
- Administration: halber Tag pro Woche, ~**2600CHF** pro Monat

Total Cost of Ownership: **73 450 CHF**

b) Managed Server bei einem Anbieter Ihrer Wahl,

Beschaffung:  
- Setup: ~ halber Tag, etwa **650CHF**

Betrieb (24 Monate):  
- Managed Server: [LittleGiant](https://www.hosttech.ch/server/managedserver/little-giant): 191CHF pro Monat
- Administration: viertel Tag pro Woche, ~**1320CHF** pro Monat

Total Cost of Ownership: **36'914 CHF**

c) IaaS mit Amazon EC2 (plus eigenverwalteter Middleware, also Admin-Kosten)

Beschaffung:  
- Setup: ~ halber Tag, etwa **650 CHF**

Betrieb (24 Monate):  
- EC2 Instance: `t2.large` - 10-70CHF pro Monat, je nach Auslastung
- Adminstration: viertel Tag pro Woche, ~**1320 CHF** pro Monat

Total Cost of Ownership: **32'570 CHF - 34'010 CHF**


d) ihrem Cloud-Provider. Falls Ihr Cloud-Provider Amazon ist, wählen Sie zum Vergleich einen Konkurrenten (PaaS oder IaaS).

Beschaffung:  
- Setup: ~ viertel Tag, etwa **325 CHF**

Betrieb (24 Monate):
- Jelastic Environment 57CHF - 133CHF, je nach Auslastung
- Administration: 2 Stunden pro Monat, ~**330 CHF** pro Monat

Total Cost of Ownership: **9'613 CHF -11'437 CHF**

- Welche Variante scheint am kostengünstigsten zu sein?

Jelastic bei unserem Anbieter Flow ist in dieser Berechnung am günstigsten.
Amazon ist teurer, da es durch IaaS vorraussichtlich mehr Administrationsaufwand erfordern wird.

- Auf welche Schwierigkeiten stossen Sie bei diesem Vergleich?

Für die selbst gehostete Lösung wurde ein extra Internetanschluss eingeplant, dieser könnte u.U. unnötig
Die effektive Auslastung ist nicht im Vorraus absehbar. Auch die Performance-Requirements der Applikation sind in diesem Beispiel sehr niedrig gesetzt. Mit höheren Leistungsanforderungen würden Cloud angebote noch besser darstehen. Ab einer gewissen Menge an Performance kippt dies jedoch wieder und eine Abhängigkeit von Amazon kann sehr teuer werden (siehe z.B. Lyft, $8 Mio. pro Monat).

- Wie schätzen Sie die Preise für Cloud Computing insgesamt ein?

Auf den ersten Blick günstig. Jedoch gibt es bei einigen Anbietern versteckte Kosten und zusätzliche Probleme, was bei einer stark ausgelasteten Applikation zu enormen Kosten führen kann. Bei einem erfolgreichen Produkt wird der gewählt Cloudanbieter am Profit beteiligt, und Vendor Lock-In führt zu einer einseitigen Abhängigkeit des Nutzers vom Cloud Angebot. Wenn man dem entkommen möchte, dann könnte man z.B. Container einsetzen, wodurch jedoch auch wieder mehr Administrationsaufwand enstehen könnte.
Unser Cloud Anbieter fällt hier ziemlich gut auf, da er zwar auf den ersten Blick höhere Kosten hat, es jedoch keine versteckte Kosten für z.B. Traffic gibt. Auch die Benutzung von jelastic verhindert übermässigen Lock-In, da Flow nicht der einzige Anbieter des PaaS Angebotes ist.

- Sollte man den Cloud-Anbieter rein nach gebotener Funktionalität und Preis
  auswählen – oder gibt es weitere Entscheidungskriterien?

Unser Cloudanbieter ist in der Schweiz in ehemaligen Atom-Luftschutzbunkern. Durch die Nutzung einer ehemaligen Militärinstallation ist nicht nur der ultimative Schutz der Daten gewährleistet, auch der Personenverkehr zu und von den Bunkeranlagen unterliegt Auflagen, wodurch Fremden der Zugang zu den gelagerten Daten erschwert wird. Verglichen mit anderen Rechenzentren, wo man "nur" durch ein wenig Luft und ein paar Wänden kommen müsste.

- Würden Sie den Cloud-Provider einsetzen für das produktive Deployment einer
  solchen Enterprise Application oder bei klassischem Hosting bleiben? Warum?

Flow wäre eine gute Alterntative für Schweizer Betriebe, um ihre Applikationen in der Cloud zu deployen. Keine Versteckten kosten, flexible Skalierung und eine Präzens vor Ort mit schnellem Support in der Landessprache ist viel wert.